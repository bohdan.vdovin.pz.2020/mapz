namespace Lab1{
    public class Shark : MarineCreature{
        private string sound = "Cluch";
        public override string ProduceSound(){
            return $"{sound}, {sound}, {sound}";
        }
    }
}