namespace Lab1.Task{
    struct HostStruct{
        public string Hostname{get;}
        public UInt32 IpV4{get;}
        public UInt32 SubnetMask{get;}
        public UInt32 BroadCastAddresss(){
            var ip = IpV4;
            var subnetmask = SubnetMask;
            return ip | (~subnetmask);
        }
        public HostStruct(string hostname, UInt32 ip, UInt32 subnetmask){
            Hostname = hostname;
            IpV4 = ip;
            SubnetMask = subnetmask;
        }
    }
}