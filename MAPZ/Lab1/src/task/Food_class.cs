namespace Lab1.Task{
    class FoodClass : IEatalbe{
        public string Name{get;}
        public int Calories{get;}
        public FoodClass(string name, int calories){
            Name = name;
            Calories = calories;
        }
    }
}