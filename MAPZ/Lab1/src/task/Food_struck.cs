namespace Lab1.Task{
    struct FoodStruct: IEatalbe{
        public string Name{get;}
        public int Calories{get;}
        public FoodStruct(string name, int calories){
            Name = name;
            Calories = calories;
        }
    }
}