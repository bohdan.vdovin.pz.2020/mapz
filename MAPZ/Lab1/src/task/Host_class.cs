namespace Lab1.Task{
    class HostClass{
        public string Hostname{get;}
        public UInt32 IpV4{get;}
        public UInt32 SubnetMask{get;}
        public UInt32 BroadCastAddresss(){
            var ip = IpV4;
            var subnetmask = SubnetMask;
            return ip | (~subnetmask);
        }
        public HostClass(string hostname, UInt32 ip=0, UInt32 subnetmask=0){
            Hostname = hostname;
            IpV4 = ip;
            SubnetMask = subnetmask;
        }
    }
}