namespace Lab1
{
    public class Mermaid : MarineCreature, IHuman
    {
        private string sound = "OhOh";
        private string endSound;
        public string Dance()
        {
            return "Move, move, vom, wom";
        }
        public override string ProduceSound()
        {
            return $"{sound},{sound},{sound},{endSound}";
        }
        public override string Move()
        {
            return "Swim, go, swim, go";
        }
        public Mermaid(int n, string endSound = "bye") : base(n)
        {
            this.endSound = endSound;
        }
    }
}