namespace Lab1{
    public class Book{
        public double width;
        protected double height;
        private string title;
        private protected string author;
        internal protected int year;
        internal long copies;
        
        public Book(double width, double height, string title, string author, int year, long copies){
            this.width = width;
            this.height = height;
            this.title = title;
            this.author = author;
            this.year = year;
            this.copies = copies;
        }
        
        public static explicit operator double(Book a){
            return a.copies;
        }
        public static implicit operator string(Book a){
            return a.title;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;
            return obj.GetHashCode() == this.copies;
        }
        public override int GetHashCode()
        {
            return (int)this.copies;
        }
        public override string ToString(){
            return this.title;
        }

    }
}