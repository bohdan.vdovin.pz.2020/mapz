namespace Lab1{
    public abstract class MarineCreature : ICreature {
        private string steps = "Swim, Swim, Swim";
        private int n;
        abstract public string ProduceSound();
        virtual public string Move(){
            string res="";
            for(int i=0; i < n; i++){
                res+=steps;
            }
            return res;
        }
        public MarineCreature(int n=1){
            this.n = n;
        }
    }
}