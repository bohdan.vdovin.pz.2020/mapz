namespace Lab1{
    enum MissleCode: byte{
        ROCKET,
        BULLET,
        WATER,
        PLACEHOLDER,
    };
}