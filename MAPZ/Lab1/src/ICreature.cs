namespace Lab1
{
    public interface ICreature
    {
        public string ProduceSound();
        public string Move();

    }
}
