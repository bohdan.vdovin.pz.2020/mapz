namespace Lab1
{
    public class Plant
    {
        protected string greet = "Greetings";
        public string Introduce()
        {
            return greet + " I'm a regular plant";
        }
    }
    public class Oak : Plant
    {
        public override string ToString()
        {
            return "I'm an Oak instance";
        }
    }
    public class Roze : Plant
    {
        public override string ToString()
        {
            return "I'm an Roze instance";
        }
    }
}