﻿using Lab1;
using Lab1.Task;
using System.Diagnostics;
using System.Reflection;

class Program{
    static public void Main(string [] argv){
        long size = 10000000;
        Stopwatch stopWatch = new Stopwatch();

        List<Lab1.Task.FoodClass> lst1 = new List<Lab1.Task.FoodClass>();
        List<Lab1.Task.FoodStruct> lst2 = new List<Lab1.Task.FoodStruct>();
        List<Lab1.Task.HostClass> lst3 = new List<Lab1.Task.HostClass>();
        List<Lab1.Task.HostStruct> lst4 = new List<Lab1.Task.HostStruct>();
        
        stopWatch.Start();
        for(int i=0; i < size; ++i){
            lst1.Add(new Lab1.Task.FoodClass("Orange",160));
        }
        stopWatch.Stop();
        Console.WriteLine(stopWatch.ElapsedMilliseconds);
        stopWatch.Reset();

        stopWatch.Start();
        for(int i=0; i < size; ++i){
            lst2.Add(new Lab1.Task.FoodStruct("Orange",160));
        }
        stopWatch.Stop();
        Console.WriteLine(stopWatch.ElapsedMilliseconds);
        stopWatch.Reset();

        stopWatch.Start();
        for(int i=0; i < size; ++i){
            lst3.Add(new Lab1.Task.HostClass("localhost",0x7f000001, 0xffffff00));
        }
        stopWatch.Stop();
        Console.WriteLine(stopWatch.ElapsedMilliseconds);
        stopWatch.Reset();

        stopWatch.Start();
        for(int i=0; i < size; ++i){
            lst4.Add(new Lab1.Task.HostStruct("localhost",0x7f000001, 0xffffff00));
        }
        stopWatch.Stop();
        Console.WriteLine(stopWatch.ElapsedMilliseconds);
        Console.ReadLine();
        // Task10();
        
    }
    public static void Task10(){
        Type? magicType = Type.GetType("Lab1.Task.HostClass");
        if(magicType == null)
        {
            Console.WriteLine("Null");
            System.Environment.Exit(1);
            
        }

        ConstructorInfo? magicConstructor = magicType.GetConstructor(new Type[]{typeof(string),typeof(UInt32), typeof(UInt32)});
        if(magicConstructor == null){
            Console.WriteLine("Null1");
            System.Environment.Exit(2);
        }

        object? magicClassObject = magicConstructor.Invoke(new object[]{"localhost",(UInt32)0x7f000001,(UInt32) 0xffffff00});
        if(magicClassObject == null){
            Console.WriteLine("Null2");
            System.Environment.Exit(3);
        }

        MethodInfo? magicMethod = magicType.GetMethod("BroadCastAddresss");
        object? magicValue = magicMethod.Invoke(magicClassObject, new object[]{});

        Console.WriteLine("MethodInfo.Invoke() Example\n");
        Console.WriteLine("MagicClass.ItsMagic() returned: {0}", Convert.ToString((UInt32)magicValue,16));
    }

    public static void Task9(){
        Book mybook = new Book(10, 53, "The History of Humanity", "Tom", 1984, 10000);
        Console.WriteLine(mybook.GetHashCode());
        Console.WriteLine(mybook.ToString());
        Console.WriteLine(mybook.Equals(new Book(0,0,"","",433,10000)));
    }
    public static void Task8(){
        Lab1.Book mybook = new Lab1.Book(10, 53, "The History of Humanity","Tom",1984,10000);
        string title = mybook;
        double copies = (double)mybook; 
    }
    public static void Task7(){
        Plant tree1 = new Oak();
        Plant tree2 = new Roze();
        object tree1Obj = tree1;
        object tree2Obj = tree2;
        Console.WriteLine(tree1Obj.ToString());
        Console.WriteLine(tree2Obj.ToString());
        var tree1_ = (Plant)tree1Obj;
        var tree2_ = (Plant)tree2Obj;
        Console.WriteLine(tree1_.Introduce());
        Console.WriteLine(tree2_.Introduce());
    }
    public static void Task6(){
        int a = 10;
        int b = 15;
        //Ref matter
        Console.WriteLine($"Begin: a: {a}, b: {b}.");
        SwapRef(ref a, ref b);
        Console.WriteLine($"Ref swap: a: {a}, b: {b}.");
        SwapWithoutRef(a, b);
        Console.WriteLine($"Without ref swap: a: {a}, b: {b}.");
        //Ref doesn't matter
        Console.WriteLine($"Max ref: {MaxRef(ref a,ref b)}. a: {a},b: {b}.");
        Console.WriteLine($"Max without swap: {MaxWithoutRef(a, b)}. a: {a},b: {b}.");
        //Out matter
        int res;
        MinOut(a, b, out res);
        Console.WriteLine($"Res out: {res}");
        res = 0;
        MinWithoutOut(a, b, res);
        Console.WriteLine($"Res without out: {res}");
    }
    public static void Task5(){
        Base obj = new Derived();
    }
    public static void Task4(){
        Console.WriteLine(Lab1.MissleCode.BULLET ^ Lab1.MissleCode.WATER);
        Console.WriteLine(Lab1.MissleCode.PLACEHOLDER & Lab1.MissleCode.BULLET);
        Console.WriteLine(Lab1.MissleCode.WATER | Lab1.MissleCode.ROCKET);
    }
    public static void Task2(){
        // Lab1.Book mybook = new Lab1.Book(10, 53, "The History of Humanity");
        // Console.WriteLine(mybook.width);
        // Console.WriteLine(mybook.height);
        // Console.WriteLine(mybook.title);
        // Console.WriteLine(mybook.author);
        // Console.WriteLine(mybook.year);
        // Console.WriteLine(mybook.copies);
    }
    
    public static void SwapRef(ref int a, ref int b){
        int temp = a;
        a = b;
        b = temp;
    }

    public static void SwapWithoutRef(int a, int b){
        int temp = a;
        a = b;
        b = temp;
    }
    
    public static int MaxRef(ref int a, ref int b){
        return a < b ? b : a;
    }

    public static int MaxWithoutRef(int a, int b){
        return a < b ? b : a;
    }
    
    public static void MinOut(int a, int b, out int res){
        res = a < b ? a : b;
    }

    public static void MinWithoutOut(int a, int b, int res){
        res = a < b ? a : b;
    }
}