namespace Game{
    public class MonsterRegistry{
        protected List<Monster> items;
        public IEnumerable<Monster>? getElelementsByName(string name){
            var res = items.Where(m => m.Name==name);
            if (res.Any()){
                return null;
            }
            return res;
        }
        public MonsterRegistry(){
            items = new List<Monster>();

            // Just for the sake of not leaving registry empty
            Monster goblin = new Monster("Goblin",-1, -1, 10, 1, new MockView("Goblin"));
            Monster headlessKazap = new Monster("Headless Kazap", -1, -1, 1, 0, new MockView("Headless Kazap"));
            Monster dragon = new Monster("Dragon", -1, -1, 100, 30, new MockView("Dragon"));
            Monster wolf = new Monster("Wolf", -1, -1, 12, 3, new MockView("Wolf"));
        }
        public void AddMonster(Monster monster){
            items.Add(monster);
        }
    }

}