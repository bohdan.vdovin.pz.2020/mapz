namespace Game{
    public interface IView{
        public string Representation();
    }
}