namespace Game
{
    public class FireMorturaTower: MorturaTower
    {
        public override void Attack(Monster monster) { throw new NotImplementedException(); }
        public override void CreateEffect() { throw new NotImplementedException(); }
        public override void DestoroyEffect() { throw new NotImplementedException(); }

        public FireMorturaTower
        (int x, int y, int maxHealth, int maxDamage, int maxAttackSpeed, Unit attackRadious, Unit splashRadious, int cost):
        base(x, y, maxHealth, maxDamage, maxAttackSpeed, attackRadious, splashRadious, cost)
        {

        }
    }
}
