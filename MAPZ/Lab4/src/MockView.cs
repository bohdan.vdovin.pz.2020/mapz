namespace Game{
    public class MockView : IView{
        public string Name{get; init;}
        public string Representation(){
            return $"I'm a {Name}. Fear me, because I'm going to destory the city.";
        }
        public MockView(string name){
            Name = name;
        }
    }
}