namespace Game
{
    public abstract class WizardTower
    {
        // Similar to archer tower but is more expensive and have more unique spells, also 
        public int X { get; protected set; }
        public int Y { get; protected set; }
        public int MaxHealth { get; protected set; }
        public int Health { get; protected set; }
        public int MaxDamage { get; protected set; }
        public int Damage { get; protected set; }
        public int MaxAttackSpeed { get; protected set; }
        public int AttackSpeed { get; protected set; }
        public Unit AttackRadious { get; protected set; }
        public int Cost { get; protected set; }

        public abstract void Attack(Monster monster);
        public abstract void CreateEffect();
        public abstract void DestoroyEffect();

        public WizardTower(int x, int y, int maxHealth, int maxDamage, int maxAttackSpeed, Unit attackRaious, int cost)
        {
            X = x;
            Y = y;
            MaxHealth = maxHealth;
            Health = maxHealth;
            MaxDamage = maxDamage;
            Damage = maxDamage;
            MaxAttackSpeed = maxAttackSpeed;
            AttackRadious = attackRaious;
            Cost = cost;
        }
    }
}