namespace Game
{
    public abstract class KnightTower : ITower
    {
        // Knights are 
        public int X{get; protected set;}
        public int Y{get; protected set;}
        public int MaxHealth { get; protected set; }
        public int Health { get; protected set; }
        public int MinSpawnTime { get; protected set; }
        public int SpawnTime { get; protected set; }
        public int KnightNumber { get; protected set; }
        public Unit AttackRadious{ get; protected set; }
        public int Cost{ get; protected set;}

        // TODO: Add knights

        public abstract void Attack(Monster monster);
        public abstract void CreateEffect();
        public abstract void DestoroyEffect();
        public abstract void InitializeKnights();

        public KnightTower(int x, int y, int maxHealth, int minSpawnTime, int knightsNumber,Unit attackRadious, int cost)
        {
            X = x;
            Y = y;
            MaxHealth = maxHealth;
            Health = maxHealth;
            MinSpawnTime = minSpawnTime;
            SpawnTime = minSpawnTime;
            KnightNumber = knightsNumber;
            AttackRadious = attackRadious;
            Cost = cost;
        }
        
        protected List<TowerKnight>? knights = null;
    }
}