namespace Game{
    public class Monster: ICloneable{
        //Position on the map
        public string Name{get; protected set;}
        public int X{get; protected set;}
        public int Y{get; protected set;}
        // Health
        public int MaxHealth {get; protected set;}
        public int Health {get; protected set;}
        // Damage
        public int MaxDamage {get; protected set;}
        public int Damage {get; protected set;}
        // View on the map
        public IView View {get; protected set;}
        // TODO: Add actuall methods for the monster

        public Monster(string name, int x, int y, int maxHealth, int maxDamage, IView view){
            Name = name;
            X = x;
            Y = y;
            MaxHealth = maxHealth;
            Health = maxHealth;
            MaxDamage = maxDamage;
            Damage = maxDamage;
            View = view;
        }
        
        public object Clone(){ 
            return new Monster(Name, X, Y, MaxHealth, MaxDamage, View);
        }
    }
}