namespace Game{
    public abstract class MorturaTower: ITower{
        // Do spash damage
        // Slow
        // Expensive
        public int X{get; protected set;}
        public int Y{get; protected set;}
        public int MaxHealth { get; protected set; }
        public int Health { get; protected set; }
        public int MaxDamage { get; protected set; }
        public int Damage { get; protected set; }
        public int MaxAttackSpeed{ get; protected set;}
        public int AttackSpeed{ get; protected set;}
        public Unit AttackRadious{get; protected set;} 
        public Unit SpashRadius{ get; protected set;}
        public int Cost{ get; protected set;}
        // TODO: Add knights

        public abstract void Attack(Monster monster);
        public abstract void CreateEffect();
        public abstract void DestoroyEffect();

        public MorturaTower(int x, int y, int maxHealth, int maxDamage, int maxAttackSpeed, Unit attackRadious, Unit splashRadious, int cost)
        {
            X = x;
            Y = y;
            MaxHealth = maxHealth;
            Health = maxHealth;
            MaxDamage = maxDamage;
            Damage = maxDamage;
            MaxAttackSpeed = maxAttackSpeed;
            AttackSpeed = maxAttackSpeed;
            AttackRadious = attackRadious;
            SpashRadius = splashRadious;
            Cost = cost;
        }
    }
}