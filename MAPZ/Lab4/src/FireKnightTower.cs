namespace Game
{
    public class FireKnightTower : KnightTower
    {
        public override void Attack(Monster monster) { throw new NotImplementedException(); }
        public override void CreateEffect() { throw new NotImplementedException(); }
        public override void DestoroyEffect() { throw new NotImplementedException(); }
        public override void InitializeKnights() { throw new NotImplementedException(); }

        public FireKnightTower
        (int x, int y, int maxHealth, int minSpawnTime, int knightsNumber, Unit attackRadious, int cost) :
        base(x, y, maxHealth, minSpawnTime, knightsNumber, attackRadious, cost)
        {

        }
    }
}