namespace Game
{
    public class FireArcherTower : ArcherTower
    {
        public override void Attack(Monster monster) { throw new NotImplementedException(); }
        public override void CreateEffect() { throw new NotImplementedException(); }
        public override void DestoroyEffect() { throw new NotImplementedException(); }

        public FireArcherTower
        (int x, int y, int maxHealth, int maxDamage, int maxAttackSpeed, Unit attackRadious, int cost):
        base(x, y, maxHealth, maxDamage, maxAttackSpeed, attackRadious, cost)
        {

        }
    }
}