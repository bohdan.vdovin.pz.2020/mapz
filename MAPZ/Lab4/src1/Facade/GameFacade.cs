namespace Game{
    public class GameFacade{
        public static void ScatterMonsters(IEnumerable<AMonster> monsters, int x, int y, int factor = 5)
        {
            long seed = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var rand = new Random((int)seed);
            foreach(var monster in monsters)
            {
                int new_x = rand.Next(1, factor);
                int new_y = rand.Next(1, factor);
            }
        }
        public static void DestroyAllMonsters(IEnumerable<AMonster> monsters){
            foreach(var monster in monsters){
                monster.TakeDamage(monster.Health);
            }
        }
    }
}