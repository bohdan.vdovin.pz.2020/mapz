namespace Game{
    public class RageMonster: MonsterDecorator {
        public int DamagePercentageIncrease{get;}
        public RageMonster(AMonster monster, int percentage): base(monster){
            DamagePercentageIncrease = percentage;
        }
        public override int Damage {get{ return this.wrappee.Damage * DamagePercentageIncrease; }}
        public override int MaxDamage {get{ return this.wrappee.Damage * DamagePercentageIncrease; } }
    }
}