namespace Game{
    public interface IDamageble {
        public int Damage {get;} 
        public int MaxDamage {get;}
    }
}