namespace Game{
    public abstract class MonsterDecorator: IDamageble{
        public abstract int Damage{get;}
        public abstract int MaxDamage{get;}
        public MonsterDecorator(AMonster monster){
            this.wrappee = monster;
        }
        protected AMonster wrappee;
    }
}