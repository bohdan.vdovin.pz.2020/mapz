namespace Game{
    public class PoisendMonster: MonsterDecorator {
        public int DamagePercentegeReduce{get;}
        public PoisendMonster(AMonster monster, int percentage): base(monster){
            DamagePercentegeReduce = percentage;
        }
        public override int Damage {get{ return this.wrappee.Damage * DamagePercentegeReduce; }}
        public override int MaxDamage {get{ return this.wrappee.Damage * DamagePercentegeReduce; } }
    }
}