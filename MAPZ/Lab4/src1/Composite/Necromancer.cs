namespace Game
{
    public class Necromancer : AMonster
    {
        public override bool addMonster(AMonster monster) {subordinates.Add(monster); return true;}
        public override bool removeMonster(AMonster monster) {return subordinates.Remove(monster);}
        public override string getInfo()
        {
            string res = View.Representation() + '\n';
            foreach (var monster in subordinates)
            {
                res += "{Subordinate:" + monster.getInfo() + "}\n";
            }
            return res;
        }
        public override AMonster getMonsterById(int id) { throw new NotImplementedException(); }

        public Necromancer(int id, string name, int x, int y, int maxHealth, int maxDamage, IView view) :
        base(id, name, x, y, maxHealth, maxDamage, view)
        {
            subordinates.AddRange(new []{
                new Skeleton(Game.GetUniqueID(), "Skeleton1", 0,0,10,5,new MockView("Skeleton")),
                new Skeleton(Game.GetUniqueID(), "Skeleton2", 0,0,10,5,new MockView("Skeleton")),
                new Skeleton(Game.GetUniqueID(), "Skeleton3", 0,0,10,5,new MockView("Skeleton"))}
            );
            for(int i =0; i < subordinates.Count; i++)
            {
                subordinates[i].addMonster(new Skull(Game.GetUniqueID(), $"Skull{i}1", 0,0,10,5,new MockView("Skull")));
                subordinates[i].addMonster(new Skull(Game.GetUniqueID(), $"Skull{i}2", 0,0,10,5,new MockView("Skull")));
                subordinates[i].addMonster(new Skull(Game.GetUniqueID(), $"Skull{i}3", 0,0,10,5,new MockView("Skull")));
            }
        }
        private List<AMonster> subordinates = new List<AMonster>();
    }
}