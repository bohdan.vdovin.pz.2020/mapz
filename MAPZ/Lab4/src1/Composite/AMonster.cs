namespace Game{
    public abstract class AMonster:IDamageble{
        //Position on the map
        public int ID{get; protected set;}
        public string Name{get; protected set;}
        public int X{get; protected set;}
        public int Y{get; protected set;}
        // Health
        public int MaxHealth {get; protected set;}
        public int Health {get; protected set;}
        // Damage
        public int MaxDamage {get; protected set;}
        public int Damage {get; protected set;}
        // View on the map
        public IView View {get; protected set;}

        public AMonster(int id, string name, int x, int y, int maxHealth, int maxDamage, IView view){
            ID = id;
            Name = name;
            X = x;
            Y = y;
            MaxHealth = maxHealth;
            Health = maxHealth;
            MaxDamage = maxDamage;
            Damage = maxDamage;
            View = view;
        }
        
        public abstract string getInfo();
        public abstract bool addMonster(AMonster monster);
        public abstract bool removeMonster(AMonster monster);
        public abstract AMonster getMonsterById(int id);
        public void TakeDamage(int damage){
            Health = Health < damage? 0: Health - damage; 
        }
    }
}