namespace Game
{
    public class Skeleton : AMonster
    {
        public override bool addMonster(AMonster monster) {subordinates.Add(monster); return true;}
        public override bool removeMonster(AMonster monster) {return subordinates.Remove(monster);}
        public override string getInfo()
        {
            string res = View.Representation() + '\n';
            foreach (var monster in subordinates)
            {
                res += "{Subordinate:" + monster.View.Representation() + "}\n";
            }
            return res;
        }
        public override AMonster getMonsterById(int id) { throw new NotImplementedException(); }

        public Skeleton(int id, string name, int x, int y, int maxHealth, int maxDamage, IView view) :
        base(id, name, x, y, maxHealth, maxDamage, view)
        { }
        private List<AMonster> subordinates = new List<AMonster>();
    }
}