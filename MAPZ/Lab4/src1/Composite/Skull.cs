namespace Game{
    public class Skull: AMonster{
        public override bool addMonster(AMonster monster){ throw new NotImplementedException(""); }
        public override bool removeMonster(AMonster monster){ throw new NotImplementedException(""); }
        public override string getInfo(){
            return View.Representation();
        }
        public override AMonster getMonsterById(int id) { throw new NotImplementedException(); }

        public Skull(int id, string name, int x, int y, int maxHealth, int maxDamage, IView view) :
        base(id, name, x, y, maxHealth, maxDamage, view)
        { }
    }
}