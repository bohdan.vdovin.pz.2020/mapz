namespace Game{
    public abstract class TowerKnight{
        public int X{get; protected set;}
        public int Y{get; protected set;}
        public int MaxHealth{get; protected set;}
        public int Health{get; protected set;}
        public int MaxDamage{get; protected set;}
        public int Damage{get; protected set;}
        public int MaxAttackSpeed{get; protected set;}
        public int AttackSpeed{get; protected set;}

        public abstract void Attack(Monster monster);

        public TowerKnight(int x, int y, int maxHealth, int maxDamage, int maxAttackSpeed){
            X = x;
            Y = y;
            MaxHealth = maxHealth;
            Health = maxHealth;
            MaxDamage = maxDamage;
            MaxAttackSpeed = maxAttackSpeed;
            AttackSpeed = maxAttackSpeed;
        }
    }
}