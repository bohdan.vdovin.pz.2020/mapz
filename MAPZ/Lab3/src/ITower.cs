namespace Game{
    public interface ITower{
        public void Attack(Monster monster);
        public void DestoroyEffect();
        public void CreateEffect();
    }
}