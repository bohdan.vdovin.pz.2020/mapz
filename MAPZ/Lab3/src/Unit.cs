namespace Game{
    public class Unit{
        public int Value{get; set;}
        public Unit(int value){
            Value = value;
        }
        public Unit(){
            Value = 1;
        }
    }
}