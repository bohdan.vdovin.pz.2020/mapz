namespace Game{
    // Singelton
    public class TowerHall{
        public int MaxHealth {get; protected set;}
        public int Health {get; protected set;}
        public int Level {get; protected set;}

        private static TowerHall? obj = null;
        public static TowerHall getInstance(){
            if (obj == null){
                obj = new TowerHall(20, 20,1);
            }
            return obj;
        }
        private TowerHall(int maxHealth, int health, int level){
            MaxHealth = maxHealth;
            Health = maxHealth;
            Level = level;
        }
    }
}