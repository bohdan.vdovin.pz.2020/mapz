namespace Game{
    interface ITowersFactory{
        public ArcherTower MakeArcherTower(int x, int y);
        public WizardTower MakeWizardTower(int x, int y);
        public KnightTower MakeKnightTower(int x, int y);
        public MorturaTower MakeMorturaTower(int x, int y);
    }
}