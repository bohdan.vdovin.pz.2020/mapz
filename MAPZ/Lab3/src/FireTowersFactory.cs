namespace Game{
    public class FireTowerFactory : ITowersFactory {
        public ArcherTower MakeArcherTower(int x, int y){
            return new FireArcherTower(x, y, 100, 10, 6, new Unit(9), 65);
        }
        public WizardTower MakeWizardTower(int x, int y)
        {
            return new FireWizardTower(x, y, 100, 12, 5, new Unit(10), 100);
        }
        public KnightTower MakeKnightTower(int x, int y){
            return new FireKnightTower(x, y, 100, 60, 3, new Unit(9), 75);
        }
        public MorturaTower MakeMorturaTower(int x, int y){
            return new FireMorturaTower(x, y, 90, 15, 4, new Unit(7), new Unit(3), 120);
        }
    }
}