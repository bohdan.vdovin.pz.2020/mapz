namespace Game
{
    public abstract class ArcherTower : ITower
    {
        // Special ability headshot
        // have large range of attack
        // Phisical damage
        // Low cost
        public int X{get; protected set;}
        public int Y{get; protected set;}
        public int MaxHealth { get; protected set; }
        public int Health { get; protected set; }
        public int MaxDamage { get; protected set; }
        public int Damage { get; protected set; }
        public int MaxAttackSpeed { get; protected set; }
        public int AttackSpeed { get; protected set; }
        public Unit AttackRadious{ get; protected set;}
        public int Cost { get; protected set; }

        public abstract void Attack(Monster monster);
        public abstract void CreateEffect();
        public abstract void DestoroyEffect();

        public ArcherTower(int x, int y, int maxHealth, int maxDamage, int maxAttackSpeed, Unit attackRaious, int cost)
        {
            X= x;
            Y= y;
            MaxHealth = maxHealth;
            Health = maxHealth;
            MaxDamage = maxDamage;
            Damage = maxDamage;
            MaxAttackSpeed = maxAttackSpeed;
            AttackRadious = attackRaious;
            Cost = cost;
        }

    }
}