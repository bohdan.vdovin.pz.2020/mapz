namespace Lab2{
    public class PhoneComparerByAuthor : IComparer<Book>
    {
        public int Compare(Book ?a, Book ?b){
            return (a?.Author ?? "").CompareTo(b?.Author ?? "");
        }
    }
    public class PhoneComparerByPrice : IComparer<Book>
    {
        public int Compare(Book ?a, Book ?b){
            return (a?.Price ?? -1).CompareTo(b?.Price ?? -1);
        }
    }
    public class PhoneComparerByName : IComparer<Book>
    {
        public int Compare(Book ?a, Book ?b){
            return (a?.Title ?? "").CompareTo(b?.Title ?? "");
        }
    }
}