namespace Lab2
{
    static public class Extensions
    {
        public static Book MostRecentBook(this IEnumerable<Book> collection)
        {
            Book? res = collection.MaxBy(p => p.Year);
            if (res == null)
            {
                throw new ArgumentException("");
            }
            return res;
        }
        public static Book LeastRecentBook(this IEnumerable<Book> collection)
        {
            Book? res = collection.MinBy(p => p.Year);
            if (res == null)
            {
                throw new ArgumentException("");
            }
            return res;
        }
        public static Book MostExpensive(this IEnumerable<Book> collection)
        {
            Book? res = collection.MaxBy(p => p.Price);
            if (res == null)
            {
                throw new ArgumentException("");
            }
            return res;
        }
        public static Book LeastExpensive(this IEnumerable<Book> collection)
        {
            Book? res = collection.MinBy(p => p.Price);
            if (res == null)
            {
                throw new ArgumentException("");
            }
            return res;
        }
    }
}