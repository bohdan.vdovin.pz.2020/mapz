using Xunit;
using Lab2;
namespace Lab2Tests;

public class UnitTest1
{
    private readonly Book[] Data;
    public UnitTest1()
    {
        Data = new Book[] {
            new Book ("Karl Swedberg and Jonathan Chaffer", "Learning jQuery Fourth Edition", 312, 2010, 123.5, "Programming"),
            new Book ("Aear Bibeault and Yehuda Katz", "jQuery in Action", 513, 2007, 26.5, "Programming"),
            new Book ("Selim Arsever", "jQuery Game Development Essentials", 123, 2004, 51.99, "Programming"),
            new Book ("Goswami, Jaideva", "Fundamentals of Wavelets", 132, 1994, 13.5, "Other"),
            new Book ("Foreman, John", "Data Smart", 596, 1985, 123.5, "Data Science"),
            new Book ("Hawking, Stephen", "God Created the Integers", 341, 1980, 123.5, "Math"),
            new Book ("Dubner, Stephen", "Superfreakonomics", 1212, 1987, 123.5, "Fantasy"),
            new Book ("Said, Edward", "Orientalism", 784, 1998, 123.5, "Science"),
            new Book ("Vapnik, Vladimir", "Nature of Statistical Learning Theory, The", 254, 1990, 123.5, "Math"),
            new Book ("Sebastian Gutierrez", "Data Scientists at Work", 234, 1999, 123.5, "Data Science"),
            new Book ("Mlodinow, Leonard", "Drunkard's Walk, The", 349, 1999, 123.5, "Novel"),
            new Book ("Menon, V P", "Integration of the Indian States", 431, 1997, 122.5, "History"),
            new Book ("Villani, Cedric", "Birth of a Theorem", 678, 2018, 123.5, "Math"),
            new Book ("Eraly, Abraham", "The Age of Wrath", 501, 2017, 123.5, "History"),
            new Book ("Kafka, Frank", "The Trial", 713, 2020, 123.5, "Novel"),
        };
    }

    [Fact]
    public void TestSelectElementByYear()
    {
        var expected = new[] { 2010, 2007, 2004, 1994 };
        var temp = Data[0..4];

        var res = temp.Select(p => p.Year);

        Assert.Equal(res, expected);
    }

    [Fact]
    public void TestSelectAllGanra()
    {
        var expected = new[] { "Programming", "Other", "Data Science", "Math", "Fantasy", "Science", "Novel", "History" };

        var res = Data.Select(p => p.Ganra).Distinct();

        Assert.Equal(res, expected);
    }

    [Fact]
    public void TestWhere1()
    {
        List<Book> expected = new List<Book>();
        foreach (var item in Data)
        {
            if (item.Year > 2000)
            {
                expected.Add(item);
            }
        }

        var res = from b in Data
                  where b.Year > 2000
                  select b;

        Assert.Equal(res, expected);
    }

    [Fact]
    public void TestWhere2()
    {
        List<Book> expected = new List<Book>();
        foreach (var item in Data)
        {
            if (item.Title.ToUpper().StartsWith('A'))
            {
                expected.Add(item);
            }
        }

        var res = Data.Where(p => p.Title.ToUpper().StartsWith('A'));

        Assert.Equal(res, expected);
    }


    [Fact]
    public void TestList1()
    {
        Book obj = new Book("Oscar Wilde", "The Picture of Dorian Gray", 230, 1890, 12.99, "Novel");
        var sample = Data[3..9].ToList();
        var expected = Data[4..7].ToList();
        expected.Add(obj);

        sample.RemoveAt(0);
        sample.RemoveRange(sample.Count() - 2, 2);
        sample.Add(obj);

        Assert.Equal(sample, expected);
    }

    // [Fact]
    // public void TestList2(){}

    [Fact]
    public void TestDict1()
    {
        Book obj = new Book("Oscar Wilde", "The Picture of Dorian Gray", 230, 1890, 12.99, "Novel");
        var expected = Data[..5].ToDictionary(b => b.Title);
        var sample = new Dictionary<string, Book>{
            {Data[1].Title, Data[1]},
            {Data[2].Title, Data[2]},
            {Data[3].Title, Data[3]},
            {obj.Title, obj}
        };

        expected.Remove("Learning jQuery Fourth Edition");
        expected.Remove("Data Smart");
        expected.Add(obj.Title, obj);

        Assert.Equal(sample.OrderBy(elm => elm.Key).ToArray(), expected.OrderBy(elm => elm.Key).ToArray());
    }

    [Fact]
    public void TestDict2()
    {
        Book expected = new Book("Hawking, Stephen", "God Created the Integers", 341, 1980, 123.5, "Math");

        Book obj = Data.LeastRecentBook();

        Assert.Equal(obj, expected);
    }

    [Fact]
    public void TestMyExtensionMethods1()
    {
        var expected = Data.MostRecentBook();

        var res = new[] { "Kafka, Frank" };

        Assert.Equal(expected, res);
    }


    [Fact]
    public void TestSort1()
    {
        var expected = new[] { new Book("Aear Bibeault and Yehuda Katz", "jQuery in Action", 513, 2007, 26.5, "Programming") };

        var res = Data.OrderBy(b => b, new BookComparerByAuthor()).Where(b => b.Author.ToUpper().StartsWith("A"));

        Assert.Equal(res, expected);
    }

    [Fact]
    public void TestSort2()
    {
        var expected = new List<Book>();
        foreach (var book in Data)
        {
            if (book.Price > 100)
            {
                expected.Add(book);
            }
        }
        expected.Sort(new BookComparerByPrice());

        var res = Data.OrderBy(b => b, new BookComparerByPrice()).Where(b => b.Price > 100);

        Assert.Equal(res, expected);
    }
    
    [Fact]
    public void TestAnon(){
        var expected = new {title=Data[0].Title,price=Data[0].Price};
        Assert.Equal(expected.title,Data[0].Title);
        Assert.Equal(expected.price,Data[0].Price);

    }
}