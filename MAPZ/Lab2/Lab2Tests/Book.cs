namespace Lab2{
    public class Book {
        public string Author {get; init;}
        public string Title {get; init;}
        public int Pages{get; set;}
        public int Year{get; set;}
        public double Price{get; set;}
        public string Ganra{get; set;}

        public Book(string author, string title, int numberPages, int year, double price, string ganra){
            Author = author;
            Title = title;
            Pages = numberPages;
            Year = year;
            Price = price;
            Ganra = ganra;
        }

        public override bool Equals(object? obj)
        {
            return obj is Book book &&
                    Author == book.Author &&
                    Title == book.Title &&
                    Pages == book.Pages&&
                    Year == book.Year &&
                    Price == book.Price &&
                    Ganra == book.Ganra; 
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Author, Title, Pages, Year, Price, Ganra);
        }

        public override string ToString()
        {
            return $"Book: {Title}; Author: {Author}; Number of pages: {Pages}; Year of release: {Year}; Price: {Price}; Ganra: {Ganra}";
        }
    }
}