namespace Lab2{
    public class BookComparerByAuthor : IComparer<Book>
    {
        public int Compare(Book ?a, Book ?b){
            return (a?.Author ?? "").CompareTo(b?.Author ?? "");
        }
    }
    public class BookComparerByPrice : IComparer<Book>
    {
        public int Compare(Book ?a, Book ?b){
            return (a?.Price ?? -1).CompareTo(b?.Price ?? -1);
        }
    }
    public class BookComparerByName : IComparer<Book>
    {
        public int Compare(Book ?a, Book ?b){
            return (a?.Title ?? "").CompareTo(b?.Title ?? "");
        }
    }
}