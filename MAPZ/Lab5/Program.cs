﻿using Game;
public static class Program
{
    public static void Main(string[] args)
    {
        // State
        Console.WriteLine("--------------------------StateDemo---------------------------------");
        Player pl = new Player(100, 1);
        pl.ToGame();
        pl.StartGame();
        pl.SuspendGame();
        pl.ToMenu();

        // Strategy
        Console.WriteLine("----------------------------StrategyDemo-----------------------------");
        ICannon[] cannons = new ICannon[] { new Freeze(), new Realse(), new Zilda(), new Recoshet(), new Shift() };
        Random r = new Random();
        Tower tower = new Tower(cannons[r.Next() % cannons.Length]);
        Console.WriteLine(tower.Shoot());
        for (int i = 0; i < 10; i++)
        {
            tower.SetCannon(cannons[r.Next() % cannons.Length]);
            Console.WriteLine(tower.Shoot());
        }

        // Observer
        Console.WriteLine("--------------------------ObserverDemo-------------------------------");
        SpellHolder holder = new SpellHolder();
        for (int i = 0; i < 5; i++)
        {
            holder.Register(new Monster(100, 10, i, holder));
        }
        ISpell[] spells = new ISpell[] { new FreezeSpell(), new PoisenSpell(), new RageSpell() };
        foreach (var spell in spells)
        {
            holder.ActivateSpell(spell);
        }
        foreach (var spell in spells)
        {
            holder.StopSpell(spell);
        }
    }
}
