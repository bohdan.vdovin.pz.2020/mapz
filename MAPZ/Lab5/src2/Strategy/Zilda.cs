namespace Game
{
    public class Zilda : ICannon
    {
        public int Capacity { get; set; }
        public int DPS { get; set; }
        public Zilda(int capacity = 70, int dps = 15)
        {
            DPS = dps;
            Capacity = capacity;
        }
        public void Fire(out string res)
        {
            res = $"Zilda strategy. Capacity: {Capacity}. DPS: {DPS}.";
        }
    }
}
