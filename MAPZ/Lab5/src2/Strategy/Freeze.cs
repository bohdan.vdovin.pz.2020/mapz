namespace Game
{
    public class Freeze : ICannon
    {
        public int Capacity { get; set; }
        public int DPS { get; set; }

        public Freeze(int capacity = 100, int dps = 5)
        {
            Capacity = capacity;
            DPS = dps;
        }
        public void Fire(out string res)
        {
            res = $"Freeze strategy. DPS: {DPS}. Capcity of the bank: {Capacity}.";
        }
    }
}
