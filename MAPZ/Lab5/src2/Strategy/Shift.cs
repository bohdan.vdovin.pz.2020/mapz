namespace Game
{
    public class Shift : ICannon
    {
        public int Range { get; set; }
        public int Damage { get; set; }
        public Shift(int damage = 60, int range = 25)
        {
            Damage = damage;
            Range = range;
        }
        public void Fire(out string res)
        {
            res = $"Shift strategy. Damage: {Damage}. Range of the cannon: {Range}.";
        }
    }
}
