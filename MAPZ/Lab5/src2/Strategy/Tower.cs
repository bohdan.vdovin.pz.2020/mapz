namespace Game
{
    public class Tower
    {
        public int Health {get; set;}
        public int MaxHealth {get; set;}
        public int Damage {get; set;}
        public int MaxDamage {get; set;}
        public int Height {get; set;}
        public int X {get; set;}
        public int Y {get; set;}
        public int Id {get; set;}
        public Tower(ICannon cannon)
        {
            this._cannon = cannon;
        }
        public string Shoot()
        {
            string res = "";
            this._cannon.Fire(out res);
            return res;
        }
        public void SetCannon(ICannon cannon)
        {
            this._cannon = cannon;
        }

        protected ICannon _cannon;
    }
}
