namespace Game
{
    public interface ICannon
    {
        public void Fire(out string res);
    }
}
