namespace Game
{
    public class Realse : ICannon
    {
        public int Damage { get; set; }
        public Realse(int damage = 50)
        {
            Damage = damage;
        }
        public void Fire(out string res)
        {
            res = $"Realse strategy. Damage: {Damage}.";
        }
    }
}
