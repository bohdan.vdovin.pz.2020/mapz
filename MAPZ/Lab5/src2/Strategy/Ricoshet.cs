namespace Game
{
    public class Recoshet : ICannon
    {
        public int BulletDamage { get; set; }
        public Recoshet(int bulletDamage = 20)
        {
            BulletDamage = BulletDamage;
        }
        public void Fire(out string res)
        {
            res = $"Recoshet strategy. Bullet damage: {BulletDamage}.";
        }
    }
}
