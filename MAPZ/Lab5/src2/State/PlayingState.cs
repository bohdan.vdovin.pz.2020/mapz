namespace Game
{
    public class PlayingState : State
    {
        public PlayingState(Player player) : base(player)
        {

        }
        public override void ClickPlay() { }
        public override void ClickSuspend() { this._player.ChangeState(new SuspendState(this._player)); }
        public override void ClickExit() { throw new NotImplementedException(); }
        public override string ToString() { return "The current state is PlayingState"; }
    }
}
