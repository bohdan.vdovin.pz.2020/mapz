namespace Game
{
    public abstract class State
    {
        public State(Player player)
        {
            this._player = player;
        }
        public abstract void ClickPlay();
        public abstract void ClickSuspend();
        public abstract void ClickExit();

        protected Player _player;
    }
}
