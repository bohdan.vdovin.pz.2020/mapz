namespace Game
{
    public class NoState : State
    {
        public NoState(Player player) : base(player) { }
        public override void ClickPlay() { throw new NotImplementedException(); }
        public override void ClickSuspend() { throw new NotImplementedException(); }
        public override void ClickExit() { throw new NotImplementedException(); }
        public override string ToString() { return "The current state is NoState."; }
    }
}
