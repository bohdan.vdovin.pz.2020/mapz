namespace Game
{
    public class ReadyMultiplayersState : State
    {
        public ReadyMultiplayersState(Player player) : base(player)
        {

        }
        public override void ClickPlay() { this._player.ChangeState(new PlayingMultiplayerState(this._player)); }
        public override void ClickSuspend() { throw new NotImplementedException(); }
        public override void ClickExit() { throw new NotImplementedException(); }
        public override string ToString() { return "The current state is ReadyMultiplayerState"; }
    }
}
