namespace Game
{
    public class Player
    {
        public int Health { get; set; }
        public int NumberOfTowers { get; set; }
        public int Level { get; set; }
        public bool InGame { get; set; }
        private System.IO.StreamWriter _log;
        public Player(int health, int level, string log = "/dev/stdout")
        {
            this._state = new NoState(this);
            this._log = new StreamWriter(log);
            Health = health;
            Level = level;
        }
        public void ToGame()
        {
            this._state = new ReadyState(this);
            this._log.WriteLine($"Player has chosen game mode {this._state}.");
            this._log.Flush();
        }
        public void ToMultiplayerGame()
        {
            this._state = new ReadyMultiplayersState(this);
            this._log.WriteLine($"Player has chosen game mode {this._state}.");
            this._log.Flush();
        }
        public void SuspendGame()
        {
            this._state.ClickSuspend();
            this._log.WriteLine($"The game has been suspended. {this._state}.");
            this._log.Flush();
        }
        public void StartGame()
        {
            this._state.ClickPlay();
            this._log.WriteLine($"The game has started. {this._state}.");
            this._log.Flush();
        }
        public void ToMenu()
        {
            this._state.ClickExit();
            this._log.WriteLine($"The game has ended. {this._state}.");
            this._log.Flush();
        }
        public void ChangeState(State state) { this._state = state; }
        private State _state;
    }
}
