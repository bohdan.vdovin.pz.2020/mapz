namespace Game
{
    public class SuspendMultiplayerState : State
    {
        public SuspendMultiplayerState(Player player) : base(player)
        {

        }
        public override void ClickPlay() { throw new NotImplementedException(); }
        public override void ClickSuspend() { }
        public override void ClickExit() { this._player.ChangeState(new NoState(this._player)); }
        public override string ToString() { return "The current state is SuspendMultiplayerState"; }
    }
}
