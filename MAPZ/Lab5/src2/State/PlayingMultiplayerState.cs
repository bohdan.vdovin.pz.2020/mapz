namespace Game
{
    public class PlayingMultiplayerState : State
    {
        public PlayingMultiplayerState(Player player) : base(player)
        {

        }
        public override void ClickPlay() { }
        public override void ClickSuspend() { this._player.ChangeState(new SuspendMultiplayerState(this._player)); }
        public override void ClickExit() { throw new NotImplementedException(); }
        public override string ToString() { return "The current state is PlayingMultiplayerState"; }
    }
}
