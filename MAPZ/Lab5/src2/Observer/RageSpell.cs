namespace Game
{
    public class RageSpell : ISpell
    {
        public int Duration { get; set; }
        public int DamageIncrease { get; set; }
        public RageSpell(int damageIncrease = 11, int duration = 10)
        {
            DamageIncrease = duration;
            Duration = duration;
        }

        public bool IsActive() { return this.isActive; }
        public void Activate() { this.isActive = true; }
        public void Stop() { this.isActive = false; }

        private bool isActive = false;

    }
}

