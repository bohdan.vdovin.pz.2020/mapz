namespace Game
{
    public class PoisenSpell : ISpell
    {
        public int Duration { get; set; }
        public int DPS { get; set; }
        public PoisenSpell(int dps = 10, int duration = 5)
        {
            Duration = duration;
            DPS = dps;
        }

        public bool IsActive() { return this.isActive; }
        public void Activate() { this.isActive = true; }
        public void Stop() { this.isActive = false; }

        private bool isActive = false;

    }
}

