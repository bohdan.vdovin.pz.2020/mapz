namespace Game
{
    class Monster : IObserver
    {
        public int Health { get; set; }
        public int Damage { get; set; }
        public int ID { get; init; }
        public Monster(int health, int damage, int id, SpellHolder holder)
        {
            Health = health;
            Damage = damage;
            this._holder = holder;
            ID = id;
        }
        public void update()
        {
            Console.WriteLine($"Spell event happened. Monster id: {ID}");
        }
        protected SpellHolder _holder;
    }
}
