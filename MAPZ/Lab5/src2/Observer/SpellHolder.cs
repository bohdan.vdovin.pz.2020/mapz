namespace Game
{
    public class SpellHolder
    {
        public SpellHolder()
        {
            this._subscribers = new List<IObserver>();
            this._spells = new List<ISpell>();
        }
        public void Register(IObserver sub)
        {
            this._subscribers.Add(sub);
        }
        public bool UnRegister(IObserver sub)
        {
            bool res = this._subscribers.Remove(sub);
            return res;
        }
        public void NotifySubs()
        {
            foreach (var subs in this._subscribers)
            {
                subs.update();
            }
        }
        public IEnumerable<ISpell> GetActiveSpell()
        {
            var res = this._spells.Where(p => p.IsActive());
            return res;
        }
        public void ActivateSpell(ISpell spell)
        {
            this._spells.Add(spell);
            spell.Activate();
            NotifySubs();
        }
        public bool StopSpell(ISpell spell)
        {
            ISpell? res = this._spells.Find(p => spell == p);
            if (res == null)
            {
                return false;
            }
            res.Stop();
            return true;
        }
        private List<ISpell> _spells;
        private List<IObserver> _subscribers;

    }
}
