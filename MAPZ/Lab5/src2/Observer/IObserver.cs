namespace Game
{
    public interface IObserver
    {
        public void update();
    }
}
