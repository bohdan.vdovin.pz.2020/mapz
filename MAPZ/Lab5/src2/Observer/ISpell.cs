namespace Game
{
    public interface ISpell
    {
        public bool IsActive();
        public void Activate();
        public void Stop();
    }
}
