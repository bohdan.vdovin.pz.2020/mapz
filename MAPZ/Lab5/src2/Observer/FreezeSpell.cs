namespace Game
{
    public class FreezeSpell : ISpell
    {
        public int Duration { get; set; }
        public int DPS { get; set; }
        public FreezeSpell(int dps = 20, int duration = 10)
        {
            Duration = duration;
            DPS = dps;
        }

        public bool IsActive() { return this.isActive; }
        public void Activate() { this.isActive = true; }
        public void Stop() { this.isActive = false; }

        private bool isActive = false;

    }
}

